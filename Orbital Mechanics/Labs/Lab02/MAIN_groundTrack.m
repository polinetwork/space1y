
close all
clear all



mu = 398600.44;


%% 
n_orb = 5;
greenwich0 = deg2rad(15);
rr_0 = [-5511.95860000000 -8221.28460000000 502.265700000000 ]';
vv_0 = [4.28400000000000  -3.70600000000000 -2.12300000000000]';

[alpha, delta, lon, lat] = groundTrack(rr_0,vv_0,   greenwich0,   n_orb,   mu, 1);



%% Ex. 2.1.b
% Plot the ground track of the following orbits:
% 
% 1. a = 83500 km, e = 0.1976, i = 60 deg, Omega = 270 deg, omega = 45 deg, theta_0 = 230 deg
% (taken from Curtis 2014, Example 4.12)
% 2. A Molniya orbit
% 3. A circular LEO orbit with different inclinations: 0 deg, 98 deg and 30 deg
% 4. A GEO orbit


%% 1.1
n_orb = 5;
a = 83500; e = 0.1976; i = deg2rad(60);
Om = deg2rad(270); om = deg2rad(45); theta_0 = deg2rad(230);

[rr_0, vv_0] = kep2car(a,e,i,Om,om,theta_0,mu);
[alpha, delta, lon, lat] = groundTrack(rr_0,vv_0,   greenwich0,   n_orb,   mu, 1);

%% 1.2
% Modify the semi-mayor axis of orbit 1 to get a
% repeating ground track after 4 periods


%% 2.
% Molnyia orbit: It is a highly elliptical orbit with an inclination
% of 63.4 degrees, an argument of perigee of 270 degrees and an orbital
% period of approximately half a sidereal day.
n_orb = 10;
sidDay = 23*3600 + 56*60 + 4;
a = (sidDay/(4*pi))^(2/3) * mu^(1/3) % a is s.t. T = (1/2)*sideralDay
e = 0.72;    i = deg2rad(63.4);
Om = deg2rad(60); om = deg2rad(270); theta_0 = 0;
[rr_0, vv_0] = kep2car(a,e,i,Om,om,theta_0,mu);
[alpha, delta, lon, lat] = groundTrack(rr_0,vv_0,   greenwich0,   n_orb,   mu, 1);



%% 3.
n_orb = 5;
a = 405 + 6378;
e = 0.0004953;
Om = deg2rad(18.5301);  om = deg2rad(26.7841);   theta_0 = 0;
i_vec = deg2rad([0, 30, 98])';
for k = 1:length(i_vec)
    i = i_vec(k);
    [rr_0, vv_0] = kep2car(a,e,i,Om,om,theta_0,mu);
    [alpha, delta, lon, lat] = groundTrack(rr_0,vv_0,   greenwich0,   n_orb,   mu, 1);
end


%% 5.
n_orb = 10;
sidDay = 23*3600 + 56*60 + 4;
a = (sidDay/(2*pi))^(2/3) * mu^(1/3);
e = 0;
Om = deg2rad(18.5301);  om = deg2rad(26.7841);   theta_0 = 0;
i_vec = deg2rad([0 5 30 98])';
for k = 1:length(i_vec)
    i = i_vec(k);
    [rr_0, vv_0] = kep2car(a,e,i,Om,om,theta_0,mu);
    [alpha, delta, lon, lat] = groundTrack(rr_0,vv_0,   greenwich0,   n_orb,   mu, 1);
end





