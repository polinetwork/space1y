
%close all
clear all

mu = astroConstants(13);

% Orbit definition
a = 29437;
% a = 32177 % modified
e = 0.451;
i = deg2rad(83.0581);
OM = deg2rad(35);
om = deg2rad(27);
th = deg2rad(30);
greenwich0 = deg2rad(0);

w_E = (2*pi + 2*pi/365.26) / (24*3600);
T = 2*pi*sqrt(a^3/mu)
n = 2*pi / (w_E * T)


% Initial state vector
[rr0, vv0] = kep2car(a,e,i,OM,om,th, mu);
y0 = [rr0(:); vv0(:)];

perturbMethod = 'cart';
C_R = 1; % Reflection coefficient
A2m = 0.5;   % A/m ratio
perturbations = {[1] [C_R A2m]};
n_orb = 30;
[alpha, delta, lon, lat, t_vec, Y] = groundTrack(rr0, vv0,   greenwich0,   n_orb,   mu, perturbations, perturbMethod);

%% 

switch perturbMethod
    case 'cart'
        for k = 1:size(Y,1);
            [a,e,i,OM,om,theta] = car2kep(Y(k,1:3),Y(k,4:6),   mu);
            kep(k,1) = a;   
            kep(k,2) = e;
            kep(k,3) = i;
            kep(k,4) = OM;
            kep(k,5) = om;
            kep(k,6) = theta;
        end
        
    case 'gauss'
            Y(:,3:6) = wrapTo2Pi(Y(:,3:6));
            % otherwise we get a plot that increases monotonically
            kep =  Y;
    otherwise
end


orbElemNames = {'a', 'e', 'i', '\Omega', '\omega', '\theta'};
orbElemUnits = {'km', '-', 'rad', 'rad', 'rad', 'rad'};
figure
set(0,'defaultTextInterpreter','latex')
for k = 1:6
    subplot(3,2,k)
    plot(t_vec,kep(:,k));
    xlabel('$t$ [s]')
    ylabel(sprintf('$%s$ [%s]', orbElemNames{k}, orbElemUnits{k}))
end
