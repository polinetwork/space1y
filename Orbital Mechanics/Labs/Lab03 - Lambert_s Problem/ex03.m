%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       ORBITAL MECHANICS                                 %
%                    Academic year 2018/2019                              %
%                                                                         %
%                    Lab 3: Lambert's problem                             %
%                          24/10/2018                                     %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc;
close all;
clear all;

% Note: read Help of lambertMR
% lambertMR(RI,RF,TOF,MU,orbitType,Nrev,Ncase,optionsLMR)
% as input you need only:
% RI,RF,TOF,MU,
% for the other parameters set:
% orbitType = 0;
% Nrev = 0;
% optionsLMR = 0;

mu = 398600.44;      % Gravitational parameter [km^3/s^2];


a_1 = 12000; e_1 = 0; i_1 = 0; OM_1 = 0; om_1 = 0;  th_1 = deg2rad(120);
a_2 = 9500; e_2 = 0.3; i_2 = 0; OM_2 = 0; om_2 = 0;  th_2 = deg2rad(250);
T_1 = 2*pi*sqrt(a_1^3/mu);
T_2 = 2*pi*sqrt(a_2^3/mu);

[RR1, VV1] = kep2car(a_1, e_1, i_1, OM_1, om_1, th_1, mu);
[RR2, VV2] = kep2car(a_2, e_2, i_2, OM_2, om_2, th_2, mu);
RR1 = RR1(:); RR2 = RR2(:); VV1 = VV1(:); VV2 = VV2(:);

y_initial_1 = [RR1; VV1];
y_initial_2 = [RR2; VV2];

ToF_min = T_1/4;
ToF_max = 3*T_2;
dt = 50; % Step size [s]
tspan_1 = [0:dt:ceil(T_1+dt)];
tspan_2 = [0:dt:ceil(T_1+ToF_max+dt)];

% Set options
options = odeset( 'RelTol', 1e-13, 'AbsTol', 1e-14 );

% Perform the integration
[~, StateMat] = ode113( @(t,y) ode_2body(t,y, mu), tspan_1, y_initial_1, options );
X = StateMat(:,1); Y = StateMat(:,2); Z = StateMat(:,3);
R1_mat = [X Y Z];
plot3(X,Y,Z)
hold on

[~, StateMat] = ode113( @(t,y) ode_2body(t,y, mu), tspan_2, y_initial_2, options );
X = StateMat(:,1); Y = StateMat(:,2); Z = StateMat(:,3);
R2_mat = [X Y Z];
plot3(X,Y,Z)

t1_vec = tspan_1;
ToF_min = ceil(ToF_min/dt)*dt; % we enforce ToF_vec to start from a multiple of dt
ToF_vec = [ToF_min:dt:ToF_max]; 


for i = 1:length(t1_vec)
    t1 = t1_vec(i);
    RR1 = R1_mat(i,:);
    for j = 1:length(ToF_vec)

        ToF = ToF_vec(j);
        RR2 = R2_mat(i+ToF_min/dt+(j-1),:); % (j-1) to account for the alreay added
                                            % index contribution corresponding to the
                                            % 1st elem. of ToF_vec, i.e.   ToF_min/dt

        [a,p,e,ERROR,VVT1,VVT2,TPAR,theta] = lambertMR( RR1, RR2 , ToF, mu, 0, 0, 2 );
        VVT1 = VVT1(:); VVT2 = VVT2(:);

        DeltaV_T1 = norm(VVT1-VV1);
        DeltaV_T2 = norm(VV2-VVT2);
        DeltaV_tot = DeltaV_T1 + DeltaV_T2;
        DeltaV_tot_mat(i,j) = DeltaV_tot;
        
        if i == 1 & j == 1
            % (i,j) = (1,1) -->   temporary assignment of optimum 
            %                     DeltaV value, for subsequent comparisons
            DeltaV_opt = DeltaV_tot;
            i_opt = i; j_opt = j; RR1_opt = RR1; RR2_opt = RR2;
        elseif DeltaV_tot < DeltaV_opt
                DeltaV_opt = DeltaV_tot;
                i_opt = i; j_opt = j; RR1_opt = RR1; RR2_opt = RR2;
        end
    end
end


%% Plot optimum Lambert's Arc

sprintf('Minimum DeltaV arc w/ cost: %f km/s', DeltaV_tot_mat(i_opt, j_opt))
ToF = ToF_vec(j_opt)
RR1 = RR1_opt
RR2 = RR2_opt
[a,p,e,ERROR,VVT1,VVT2,TPAR,theta] = lambertMR( RR1_opt, RR2_opt , ToF, mu, 0, 0, 2 );
RR1 = RR1(:); RR2 = RR2(:); VVT1 = VVT1(:); VVT2 = VVT2(:);

y0 = [RR1; VVT1];

% Set options
options = odeset( 'RelTol', 1e-13, 'AbsTol', 1e-14 );

% Set time span
tspan = [0:dt:ToF];

% Perform the integration
[T, StateMat] = ode113( @(t,y) ode_2body(t,y, mu), tspan, y0, options );

X = StateMat(:,1); Y = StateMat(:,2); Z = StateMat(:,3);
VX = StateMat(:,4); VY = StateMat(:,5); VZ = StateMat(:,6);

plot3(X,Y,Z, 'LineWidth', 3)
grid on




%% Pork-Chop Plot
figure

subplot(1,2,1)
[AX1, AX2] = meshgrid(ToF_vec, t1_vec);
surf(AX1, AX2, DeltaV_tot_mat, 'LineStyle', 'None');
xlabel('ToF [s]')
ylabel('t_1 [s]')
zlabel('\DeltaV [km/s]')
hcb = colorbar;
set(get(hcb,'Title'),'String','\DeltaV [km/s]');
shading interp

subplot(1,2,2)
contour(AX1,AX2,DeltaV_tot_mat, 100, 'LineWidth', 1);
xlabel('ToF [s]')
ylabel('t_1 [s]')
hcb = colorbar;
set(get(hcb,'Title'),'String','\DeltaV [km/s]');
%colormap(jet);
%contour(peaks);
 

 
 








