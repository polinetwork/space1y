function [T, Y] = exactSol_harmonic_oscill(t, y0, omega0, gamma)

omega = sqrt(omega0^2 - gamma^2);

T = t;


Y(:,1) = exp(-gamma*t) .* ( y0(1)*cos(omega*t) + (y0(2)+gamma*y0(1))/omega * sin(omega*t) );
Y(:,2) = exp(-gamma*t) .* ( y0(2)*cos(omega*t) - (omega0^2 * y0(1)+gamma*y0(2))/omega * sin(omega*t) );



end

