%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% H.T.T.A. - A.Y. 2018-19 - prof. M. Guilizzoni                           %
% SOLUTION BY PDE TOOLBOX AND ANALYTICAL OF A 0D TRANSIENT CONDUCTION     %
% PROBLEM                                                                 %
%                                                                         %
% BEFORE launching this file, the exercise has to be solved with          %
% PDE Toolbox (file PDETool_2019_LPA.m) and mesh and solution exported    %
% to the workspace                                                        %
% (variables: p e t for the mesh, u for the solution)                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%POSTPROCESSING OF THE NUMERICAL SOLUTION
nx=100;
ny=100;

xmin=min(p(1,:));
xmax=max(p(1,:));
ymin=min(p(2,:));
ymax=max(p(2,:));

dx=(xmax-xmin)/(nx-1);
dy=(ymax-ymin)/(ny-1);

x=xmin:dx:xmax;
y=ymin:dy:ymax;

for i=1:41
    uxy(:,:,i)=tri2grid(p,t,u(:,i),x,y);
end

for i=1:41
    TtauC(i)=uxy(50,50,i);
end

for i=1:41
    TtauE(i)=uxy(25,25,i);
end


%ANALYTICAL SOLUTION LPA
rho=8900;
c=385;
R=0.05;
T0=373.15;
Too=273.15;
h=10;

A=2*pi*R;
V=pi*R^2;

tau=0:500:20000;
TtauLPA=Too+(T0-Too)*exp(-h*A/(rho*V*c)*tau);


%CHART FOR COMPARISON
figure; hold on;

plot(tau,TtauC,'r*');
plot(tau,TtauE,'b');
plot(tau,TtauLPA,'k');

title('T(tau)');
xlabel('tau [s]');
ylabel('T [K]');

legend('T center','T external surface','T LPA');