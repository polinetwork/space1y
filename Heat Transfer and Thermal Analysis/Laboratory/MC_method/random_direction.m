%generate a random direction vector in a unit hemisphere
%author: M. Guilizzoni, Department of Energy, Politecnico di Milano
%March 2014
%
function rand_dir=random_direction(unit)

rn=rand(2);

phi=2*pi*rn(1);
theta=pi/2*rn(2);

%theta is from the normal (theta=0 -> normal)
rand_dir(1)=cos(phi)*sin(theta);
rand_dir(2)=sin(phi)*sin(theta);
rand_dir(3)=cos(theta);

%if unit = 1 => normalize normal vector
if unit==1
   mag=(sum(rand_dir.^2,2))^0.5;
   if not(mag==0)
      rand_dir=rand_dir/mag;
   end
end

end