%adapted by M.Guilizzoni - Department of Energy, Politecnico di Milano, March 2014
%from the original m-file (which was 2D and not a function) described at:
%http://stackoverflow.com/questions/14021381/matlab-generate-and-plot-a-point-cloud-distributed-within-a-triangle
%
function points=generate_points_in_triangle_fun(tv1,tv2,tv3,num_points,draw)

V = [tv1;tv2;tv3];  % # Triangle vertices, pairs of (x, y)
t = sqrt(rand(num_points,1));
s = rand(num_points,1);
points = (1 - t) * V(1, :) + bsxfun(@times, ((1 - s) * V(2, :) + s * V(3, :)), t);

%This will produce a set of points which are uniformly distributed inside the specified triangle:

if draw==1
   scatter3(points(:, 1), points(:, 2), points(:, 3), '.');
end

end