clear

tic

num_points=100;

s_tv1=[0,0,0];
s_tv2=[5,0,0];
s_tv3=[5,5,0];

t_tv1=[0,0,2];
t_tv2=[3,0,2];
t_tv3=[3,3,2];

%normal vector of the triangle, normalized
s_n_di=tri_n(s_tv1,s_tv2,s_tv3,1);

%to alter the single normal components
%s_n_di(1)=-1;

s_tri=[s_tv1;s_tv2;s_tv3];
t_tri=[t_tv1;t_tv2;t_tv3];

figure;
hold on;
grid on;

%triangle
trisurf([1 2 3],s_tri(:,1),s_tri(:,2),s_tri(:,3),'FaceColor','red','EdgeColor','none');
trisurf([1 2 3],t_tri(:,1),t_tri(:,2),t_tri(:,3),'FaceColor','green','EdgeColor','none');

points=generate_points_in_triangle_fun(s_tv1,s_tv2,s_tv3,num_points,0);

scatter3(points(:,1),points(:,2),points(:,3), 'b.');

view(60,30);
alpha(0.75);
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');

found=zeros(num_points,1);
int_xyz=zeros(num_points,3);

for p=1:num_points
    [found(p),int_xyz(p,:)]=int_ray_tri(points(p,:),s_n_di,t_tv1,t_tv2,t_tv3);   
    quiver3(points(p,1),points(p,2),points(p,3),s_n_di(1),s_n_di(2),s_n_di(3),'b-');
    scatter3(int_xyz(:,1),int_xyz(:,2),int_xyz(:,3),'k.');
end

num_int=sum(found);
fraz_int=num_int/num_points;

toc