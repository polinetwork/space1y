%Author: M. Guilizzoni, Department of Energy, Politecnico di Milano
%April 2014
%
%m-file to calculate the thermal answer of a sphere exposed to a fluid
%with known convective coefficient and oscillating undisturbed temperature,
%both with analytical solution and using finite difference approximation

clear

%sphere parameters
D=5e-3;
A=4*pi*(D/2)^2;
V=4/3*pi*(D/2)^3;
rho=8933; %Cu
c=385; %Cu
T0=393.15;

%fluid parameters
Toomax=373.15;
Toomed=293.15;
w=0.05;

%convective coefficient
%h=35; %FD starts separating
%h=40; %FD separation
%h=45; %FD large separation
%h=50; %FD diverges
%h=75; %FD diverges!
h=25; %FD OK

%plot of the analytical solution
C1=-rho*V*c/(h*A);
C2=Toomax-Toomed;

K=T0-Toomed-C2/(1+w^2*C1^2);

num_val=1000;

T=zeros(num_val,1);
Too=zeros(num_val,1);

for i=1:num_val
    t=i-1;
    num=C2*(w*C1*sin(w*t)-cos(w*t))*exp(-t/C1);
    denom=1+w^2*C1^2;
    
    T(i)=Toomed+exp(t/C1)*(K-num/denom);
    Too(i)=Toomed+(Toomax-Toomed)*sin(w*t);
end

plot(Too,'k-');
hold on;
plot(T,'r-');

%approximation using finite differences
Dtau=0.1; %time step
theta(1)=T0-Toomed;
tau0=0;

theta(2)=theta(1)+Dtau/C1*(theta(1)-C2*cos(w*tau0)); %forward scheme for the first step

for i=3:1/Dtau*num_val
    taui=(i-1)*Dtau;
    theta(i)=theta(i-2)+2*Dtau/C1*(theta(i-1)-C2*cos(w*taui));
end

Tfd=theta+Toomed;
plot(Tfd(1:1/Dtau:length(Tfd)),'b-');