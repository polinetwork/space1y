% Definizione dei parametri incerti

mu = ureal('mu',10,'Perc',20);  % uncertain gain

omn = ureal('omn',10,'Perc',80);      % uncertain natural frequency
xi = ureal('xi',0.10,'Perc',70);      % uncertain damping

% Definizione del modello nominale 
P0 = tf(10,[1 1]);

% Definizione del modello incerto
P = tf(mu,[1 1])*tf(omn^2,[1 2*xi*omn omn^2]);

% Risposte a scalino del modello incerto
figure(1),step(P,5),grid

% Diagrammi di Bode del modello incerto
figure(2),bode(P),grid

% Campionamento del modello incerto
Parray = usample(P,60);

% Scelta del modello nominale di progetto
Pn = P0;

% Definizione della funzione peso dell'incertezza dinamica equivalente
s=zpk('s');
%Wt=tf(0.2*conv([2.2500    3.0000    1.0000],[2.2500    3.0000    1.0000]),conv([1 2 1],[1 2 1]));
Wt=0.2*(1.5*s+1)^6/(0.6*s+1)^6;

% Diagrammi di Bode dell'errore relativo e della funzione peso dell'incertezza dinamica
figure(3), bodemag((Pn-Parray)/Pn,Wt,'r')

% Regolatori (PI tarati per cancellazione del polo dominante del modello
% nominale): il primo con guadagno piu' elevato, il secondo con guadagno
% ridotto
% R=tf(0.1*[1 1],[1 0])
R=tf(0.1*0.1*[1 1],[1 0])

% Funzione di trasferimento d'anello nominale 
L0=P0*R;

% Funzione di sensitivita' complementare nominale 
F0=L0/(1+L0);

% Verifica grafica della condizione di stabilita' robusta
figure(4),bode(F0,1/Wt),grid

F=P*R/(1+P*R);

Farray = usample(F,60);

figure(5),bodemag(Farray),grid

% Risposte a scalino del modello incerto in anello chiuso
figure(6),step(F,20),grid

% Definizione del peso Wp per le prestazioni nominali
Wp=tf([1 1],2*[1 0])
% Wp=tf([10 1],10*[1 0])

% Funzione di sensitivita' nominale 
S0=1/(1+L0);

% Verifica grafica della condizione di prestazioni robuste
[mag1,phase1,w]=bode(Wp*S0);

[mag2,phase2]=bode(Wt*F0,w);

figure(7),semilogx(w,20*log10(squeeze(mag1)+squeeze(mag2))),grid