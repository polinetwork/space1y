\input{mySettings}

%\includeonly{chapters/ch02}

\usepackage[english]{babel}


%% *************************** SPECIFIC NEWCOMMANDS ***************************
\newcommand{\parallelsum}{\mathbin{\!/\mkern-5mu/\!}}
% Parallel symbol


\newcommand*\circled[1]{\tikz[baseline=(char.base)]{%
            \node[circle=square,draw,inner sep=3pt] (char) {\textcolor{black}{#1}};}}
\newcommand*\rectangled[1]{\tikz[baseline=(char.base)]{%
            \node[rectangle=square,draw,inner sep=3pt] (char) {\textcolor{black}{#1}};}}

\newcommand{\example}[1]{\myhang{\circled{\textbf{\uline{Ex}}}\hspace{4ex}}#1}

\newcommand{\finding}[1]{\myhang{\rectangled{\textbf{\uline{RMK.:}}}\hspace{4ex}}#1}
\newcommand{\numof}[0]{n^{\circ}}

\newcommand{\restr}[2]{\left.#1\right|_{#2}}

\newcommand{\acron}[2]{\noindent\hbox to 0.125\textwidth{\textbf{#1}}#2}


%% *************************** SPECIFIC NEWCOMMANDS ***************************


\graphicspath{{img/}} % Set images directory




\begin{document}

\begin{center}
\Huge{\textbf{\sffamily Monte Carlo Method for Uncertainty Quantification of SRM Burning Time}} \\ \vspace{2ex} \Large{Alice Violaine Saletta} (927428) \\ \mailto{aliceviolaine.saletta@mail.polimi.it} \\ \vspace{2ex} \Large{Massimo Piazza} (919920) \\ \mailto{massimo.piazza@mail.polimi.it}
\end{center}



\subsection*{\centering Abstract}
In this report the experimental data of a Solid Rocket Motor will be analyzed for performance estimation. The lack of flexibility affecting SRMs makes the reproducibility of estimated performance a paramount aspect to the accomplishment of any mission, ranging from small sounding rockets to the solid boosters of a launch vehicle. To this aim, a \emph{Monte Carlo} simulation will be performed in order to quantify the uncertainty in terms of burning time, as well as its expected average value.
 


%\newpage
\let\cleardoublepage\clearpage % In order to avoid a blank page after nomenclature
%\tableofcontents

\nomenclature[A]{$\mu $}{Mean value \nomunit{-}}
\nomenclature[A]{$n$}{Ballistic coefficient \nomunit{-}}
\nomenclature[A]{$\sigma$}{Standard deviation \nomunit{-}}


\nomenclature[B]{$a$}{Pre-exponential coefficient \nomunit{\frac{mm}{s \cdot bar}}}
\nomenclature[B]{$r_b$}{Burning rate \nomunit{mm/s}}
\nomenclature[B]{$P_c$}{Combustion chamber pressure \nomunit{bar}}
\nomenclature[B]{$C^*$}{Characteristic velocity \nomunit{m/s}}
\nomenclature[B]{$t_b$}{Burning time \nomunit{s}}
\nomenclature[B]{$A_b$}{Burning Area \nomunit{m^2}}
\nomenclature[B]{$A_t$}{Throat Area \nomunit{m^2}}
\nomenclature[B]{$D$}{Diameter \nomunit{mm}}
\nomenclature[B]{$\mdot{p}$}{Propellant mass flow rate \nomunit{kg/s}}
\nomenclature[B]{$M_|tot|$}{Total mass \nomunit{kg}}
\nomenclature[B]{$\rho$}{Density \nomunit{kg/m^3}}
\nomenclature[B]{$M_\mathrm{tot}$}{Total propellant mass \nomunit{kg}}



%\nomenclature[C]{c}{Combustion Chamber}
%\nomenclature[C]{p}{Propellant}
%\nomenclature[C]{tot}{Total}

\nomenclature[D]{\acron{SRM}{Solid Rocket Motor}}{}
\nomenclature[D]{\acron{BATES}{\parbox[t]{4.5cm}{BAllistic Test and Evaluation System}}}{}
\nomenclature[D]{\acron{MC}{Monte Carlo}}{}
\nomenclature[D]{\acron{HTPB}{Hydroxyl-Terminated Polybutadiene}}{}



 \setlength{\columnsep}{30pt}
 \begin{multicols}{2}
 \printnomenclature
 \end{multicols}


\section{Introduction}
All the 9 propellant batches, have been tested at three different chamber pressure conditions, labeled as low-, mid-, high-pressure and characterized by the following throat diameters:

\begin{table}[H]
\centering
\begin{tabular}{@{}cccc@{}}
\toprule
Pressure Level:                                                  & \textbf{Low} & \textbf{Mid} & \textbf{High} \\ \midrule
	\midrule
\begin{tabular}[c]{@{}c@{}}$D_t$\\ $\mathrm{[mm]}$\end{tabular} & 28.80        & 25.25        & 21.81         \\ \bottomrule
\end{tabular}
\end{table}


Each batch is nominally identical and composed of: AP (68\%), Al (18\%), HTPB (14\%). The rocket motor used in each test is a BATES motor, characterized by the sizes reported in Fig. \ref{fig:BATES_schematics}.

A pressure transducer is placed inside the combustion chamber, just upstream of the grain, for measuring the pressure time-history with a sampling rate of 1000 Hz. The pressure traces resulting from the test campaign are all exhibiting a trend similar to the one reported in Fig. \ref{fig:pressureTracesSample}.


\begin{center}
\begin{minipage}{.49\textwidth}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{BATES_schematics}
    \caption{\label{fig:BATES_schematics} BATES schematics}
\end{figure}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{pressureTracesSample}
\caption{\label{fig:pressureTracesSample} Pressure traces sample}
\end{figure}
\end{minipage}
\end{center}



\section{Internal Ballistics Analysis}

The propellant burning time depends on the burning rate $r_b$ according to Vielle's law - thus under the assumption of steady state behavior -
\[r_b = a {P_{c}}^n\]
and on the burning area $A_b$. Moreover, the $A_b$ time-dependence leads to a non-constant combustion chamber pressure $P_c$ according to the following equation:
\[ P_{c} = \left(\rho_{p} a \frac{A_b}{A_t} C^{*}\right)^{\frac{1}{1-n}} \]

First, coefficients {\emph a} and {\emph n} are computed by fitting the data of all pressure traces - note that both {\emph a} and {\emph n} depend only on the propellant composition and not on the geometry of the motor. %In order to do that, for each batch and for each pressure trace, take the maximum value, then compute the 1\% and identify the action time $t_G - t_A$(FIGURA).

%Now compute reference pressure as: P_|ref| = \frac{\int_{t_A}^{t_G} P_{c}\,dt}{2 (t_G - t_A)} \] nd set the burning time $t_E - t_B$ as in FIGURA. Finally, compute the effective pressure\[P_|eff| = \frac{\int_{t_B}^{t_E} P_{c}\,dt}{t_E - t_B} \]nd the burning rate as the ratio between the thickness and the burning time\[r_b = \frac{w}{t_E - t_B}\]as to obtain a mean value for \emph{a} and \emph{n} and the respective uncertainties $\sigma$:

Secondly, the characteristic velocity can be computed from the experimental data using\footnote{From propellant composition, the average density is derived: $\rho_p = 1762 \addunit{kg/m^3}$. Then, knowing the geometry of the motor - $V_|tot| = 3.55\cdot 10^{-3} \addunit{m^3}$ - the total mass results: $M_|tot| = 6.26 \addunit{kg}$.}:
\[ C^* = \frac{\int_{0}^{t_b} P_c A_t\,dt}{M_|tot|} \]

%Assuming then a normal distribution for $a$, $n$ and $C^*$, their mean values and the standard deviations are computed as shown in Tab. \ref{tab:a_n_C_uncert}.
%
%Finally, it is necessary to study the grain regression during which $A_b$ changes - $A_t$ is instead assumed constant - and so do $P_c$ and $r_b$. By implementing an iterative method, the burning time is obtained.

% By plotting the mean burning time and its standard deviation vs number of iterations it is possible to see the convergence to the real value(?) for each motor. Low pressure motor is characterized by the highest burning time and the high pressure one with the lowest burning time (highest regression rate) (VALORI).

\section{Monte Carlo Simulation}
From the previously analyzed experimental data, the mean values along with the related uncertainty, can be computed for the three variables that will be taken into account, namely $\{a,n,C^*\}$, which yields:

\begin{table}[H]
\caption{Uncertainties computed from experimental data}
\label{tab:a_n_C_uncert}
\centering
\begin{tabular}{@{}cccc@{}}
\toprule
            & \begin{tabular}[c]{@{}c@{}}$a$\\ $\mathrm{[mm/(s \cdot bar)]}$\end{tabular} & \begin{tabular}[c]{@{}c@{}}$n$\\ $\mathrm{[-]}$\end{tabular} & \begin{tabular}[c]{@{}c@{}}$C^*$\\ $\mathrm{[m/s]}$\end{tabular} \\ \midrule \midrule
Mean value  & 1.7346                                                                      & 0.3808                                                       & 1517.5184                                                        \\
Uncertainty & $\pm 0.01861$                                                               & $\pm 0.0027535$                                              & $\pm 123.4879$                                                   \\ \bottomrule
\end{tabular}
\end{table}

We may at this point assume a \emph{Gaussian distribution} for all the variables and randomly pick 30 samples per variable, which will therefore yield a total amount of $30^3=27000$ MC iterations.
For each iteration the burning time $t_b$ will be computed by tracking the grain regression until burnout.
At each time instant the following algorithm is thus performed:
\begin{enumerate}
	\item $t = \overline{t} \quad \Rightarrow \quad {\color{bordeaux}A_b}(\overline{t}) = 2 \pi r_i (2x_|corner|) + 2 \pi (\frac{D_e^2}{4}-r_i^2)$
	\item re-compute: $\quad {\color{greenComment}P_c} \undereqtextarrow{=}{\tiny{(quasi-steady)}} \parg{ \rho_p a \frac{{\color{bordeaux}A_b}}{A_t} C^* }^{\frac{1}{1-n}} $
	\item re-compute: ${\quad \color{midbluePoliMi}r_b} = a \cdot {\color{greenComment}P_c}^n$
	\item regression progress: $\quad \Delta x \equiv \Delta y = {\color{midbluePoliMi}r_b} \cdot \Delta t$
\end{enumerate}

The algorithm will then result in a triple-loop, through which the samples are permuted. The convergence of the MC method can be checked by analyzing the trend of $\mu$ and $\sigma$, considering all the outputted values.


\begin{center}
\begin{minipage}{.49\textwidth}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{MC_lowP}
	\caption{\label{fig:MC_lowP} MC iterations (low-pressure)}
\end{figure}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{MC_mediumP}
\caption{\label{fig:MC_mediumP} MC iterations (mid-pressure)}
\end{figure}
\end{minipage}
\end{center}

\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{MC_highP}
\caption{\label{fig:MC_highP} MC iterations (high-pressure)}
\end{figure}

%\appendix
%\include{appendices/apA}


The uncertainty propagation by means of the MC method yields the following estimation:

\begin{table}[H]
\centering
\caption{Burning time uncertainty}
\label{tab:t_bUncert}
\begin{tabular}{@{}cccc@{}}
\toprule
Pressure Level:                      & \textbf{Low} & \textbf{Mid} & \textbf{High} \\ \midrule \midrule
$\overline{t}_b \, \, \mathrm{[s]}$ & 4.677       & 3.978       & 3.322        \\
$\sigma_{t_{b}} \, \, \mathrm{[s]}$ & 0.239       & 0.203       & 0.169        \\ \bottomrule
\end{tabular}
\end{table}


\section{Conclusions}
The simulation brought to the conclusion that the resulting uncertainty over the value of $t_b$, due to the propagation of the former, turns out to be around 5.1\% at all pressure levels.

It is also verified that the low pressure motor is characterized by the highest burning time and the high pressure one with the lowest burning time - i.e. highest regression rate - as expected. Indeed, the estimated values are consistent with the experimental data.


\nocite{*} % IMPORTANT: without this command, bib items which aren't already cited won't show up within your "References"

\printbibliography	

\end{document}