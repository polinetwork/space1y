% System and Jacobian calculation for T,rho equilibrium

function [fn,fjac,dKeq_dT]=jacobian_Trho(x,T,rho)

air_phys_properties;

%Compute equilibrium constants K
%K1 ->  1/2 02 -> 0
%K2 ->  1/2 N2 -> N
%K3 ->  N2 + O2 -> 2NO

[K_eq, dKeq_dT] = Keq_calc(T);

%Parameter setting
   par = 1.5 * Ru * T;
   T_inv = 1./T;
   zero  = 0.;
   one   = 1.;
   two   = 2.;

%Vibrational contribution
    vib1   = T_vibr(1) / ( exp(T_vibr(1) * T_inv) - one );
    vib2   = T_vibr(2) / ( exp(T_vibr(2) * T_inv) - one );
    vib5   = T_vibr(5) / ( exp(T_vibr(5) * T_inv) - one );

%Internal specific energy times molar mass for species
    mme_sp(1)  = Ru * ( 2.5 * T + vib1 );
    mme_sp(2)  = Ru * ( 2.5 * T + vib2 );
    mme_sp(3)  = par + heat_form_0K(3);
    mme_sp(4)  = par + heat_form_0K(4);
    mme_sp(5)  = Ru * ( 2.5 * T + vib5 ) + heat_form_0K(5);

%Cv times molar mass for species
    mmCv_sp(1) = Ru * ( 2.5 + (vib1*T_inv)^2 * exp(T_vibr(1)*T_inv) );
    mmCv_sp(2) = Ru * ( 2.5 + (vib2*T_inv)^2 * exp(T_vibr(2)*T_inv) );
    mmCv_sp(3) = 1.5 * Ru;
    mmCv_sp(4) = mmCv_sp(3);
    mmCv_sp(5) = Ru * ( 2.5 + (vib5*T_inv)^2 * exp(T_vibr(5)*T_inv) );

%Function evaluation
  fn(1) = K_eq(1) * x(1) - x(3);
  fn(2) = K_eq(2) * x(2) - x(4);
  fn(3) = K_eq(3) * x(1) * x(2) - x(5);
  fn(4) = two * rho * eta_O2_std - ( two * x(1)^2 + x(3) + x(5) );
  fn(5) = two * rho * eta_N2_std - ( two * x(2)^2 + x(4) + x(5) );

%Compute Jacobian entries
  fjac(1,1) = -K_eq(1);
  fjac(2,1) = zero;
  fjac(3,1) = -K_eq(3) * x(2);
  fjac(4,1) = 4. * x(1);
  fjac(5,1) = zero;

  fjac(1,2) = zero;
  fjac(2,2) = -K_eq(2);
  fjac(3,2) = -K_eq(3) * x(1);
  fjac(4,2) = zero;
  fjac(5,2) = 4. * x(2);

  fjac(1,3) = one;
  fjac(2,3) = zero;
  fjac(3,3) = zero;
  fjac(4,3) = one;
  fjac(5,3) = zero;

  fjac(1,4) = zero;
  fjac(2,4) = one;
  fjac(3,4) = zero;
  fjac(4,4) = zero;
  fjac(5,4) = one;

  fjac(1,5) = zero;
  fjac(2,5) = zero;
  fjac(3,5) = one;
  fjac(4,5) = one;
  fjac(5,5) = one;


return