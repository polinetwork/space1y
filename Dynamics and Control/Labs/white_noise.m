
clear all;
close all;

L = 100000;     % sample length for the random signal
mu = 0;         % mean value
W = 4;          % intensity

x = sqrt(W)*randn(L,1) + mu;    % white noise (simulated) with intensity W

figure();

subplot(3,1,1);
plot(x);
title(['White noise : \mu=',num2str(mu),' W=',num2str(W)])
xlabel('Samples')
ylabel('Sample Values')
grid on;

subplot(3,1,2);

Rxx = (1/L)*conv(flipud(x),x);    % correlation (covariance) function
lags = (-L+1):1:(L-1);

plot(lags,Rxx); 
title('Correlation (covariance) function of the white noise');
xlabel('Lags')
ylabel('Correlation (covariance)')
grid on;

% Simulating the power spectral density (PSD) of the white noise
% is a little tricky business. There are two issues here
% 1) The generated samples are of finite length.
%    This is synonymous to applying truncating an infinite series of random samples.
%    This implies that the lags are defined over a fixed range.
% 2) The random number generators used in simulations are pseudo-random generators.
% Due these two reasons, you will not get a flat spectrum of psd when you apply
% Fourier Transform over the generated correlation values.
% The wavering effect of the psd can be minimized by generating sufficiently
% long random signal and averaging the psd over several realizations of the random signal.

R = 1000; % number of realizations to average
N = 1024; % sample length for each realization set as power of 2 for FFT

MU  = mu*ones(1,N);
Cxx = W*diag(ones(N,1));
CH  = chol(Cxx); 
% Generating a Multivariate Gaussian Distribution with given mean vector and covariance Matrix Cxx
z = repmat(MU,R,1) + randn(R,N)*CH;

%By default, FFT is done across each column - Normal command fft(z)
%Finding the FFT of the Multivariate Distribution across each row
%Command - fft(z,[],2)
Z = 1/sqrt(N)*fft(z,[],2);  % scaling by sqrt(N);
Pzavg = mean(Z.*conj(Z));   % computing the mean power from fft

normFreq = [-N/2:N/2-1]/N;
Pzavg = fftshift(Pzavg);    % shift zero-frequency component to center of spectrum

subplot(3,1,3);
plot(normFreq,10*log10(Pzavg));
axis([-0.5 0.5 0 10]); grid on;
ylabel('Power Spectral Density (dB/Hz)');
xlabel('Normalized Frequency');
title('Power spectral density of the white noise (dB scale)');

